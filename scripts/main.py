import os

import requests
from deep_translator import GoogleTranslator

host_api = os.getenv('HOST_API')


def create_category(term: str) -> str:
    req = requests.post(f'{host_api}/category', json={'name': term})
    if req.status_code == 201:
        return req.json()['guid']


def list_to_products(term: str) -> list:
    req = requests.get(f'https://unsplash.com/napi/search/photos?query={term}&per_page=60&page=1')
    items_return = []
    for result in req.json()['results']:
        items_return.append({
            "name": result['description'],
            "skus": [
                {
                    "old_price": result.get('total_likes', 0),
                    "new_price": result['likes'],
                    "code": result['id']
                }
            ],
            "images": [
                {
                    "url": result['urls']['regular']
                }
            ]
        })
    return items_return


def create_product(product, category_guid):
    req = requests.post(f'{host_api}/product/{category_guid}', json=product)
    if req.status_code == 201:
        return req.text
    return req.text


if __name__ == '__main__':

    file_terms = open('./terms.txt', 'r')
    list_of_terms = (line.strip() for line in file_terms.readlines())
    file_terms.close()
    if not os.path.exists('./data'):
        os.mkdir('./data')

    translated = GoogleTranslator(source='portuguese', target='english')
    for term_raw in list_of_terms:
        file_products = open(f'./data/{term_raw}.json', 'w')
        file_products.write('[')

        category_name = translated.translate(term_raw)

        category_guid = create_category(category_name)
        for product in list_to_products(term=category_name):
            file_products.write(create_product(product, category_guid=category_guid) + ",\n")
        file_products.write(']')
        file_products.close()
    exit(0)
