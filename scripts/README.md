# Carregar produto na base de dados

Carregue uma base de 64 categorias e cerca de 400 produtos a uma base de dados para exemplo com base na API do unsplash

### HOW USE

Informe os termos no arquivo terms.txt sendo um por linha a serem procurado

### HOW RUN THIS PROJECT

    docker build -t loadproducts .

    docker run -v $PWD:/terms.txt -e HOST_API='http://127.0.0.1:3000' loadproducts