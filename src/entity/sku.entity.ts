import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ProductEntity } from './product.entity';
import { Exclude } from 'class-transformer';

@Entity()
export class SkuEntity {
  @PrimaryGeneratedColumn()
  @Exclude()
  id: number;

  @Column('varchar', { nullable: true })
  variant_name: string;

  @Column('varchar', { nullable: false, length: 16 })
  code: string;

  @Column('int', { nullable: false, default: 0 })
  old_price: number;

  @Column('int', { nullable: false })
  new_price: number;

  @Column('boolean', { default: true })
  is_publish: boolean;

  @CreateDateColumn()
  @Exclude()
  date_created: Date;

  @UpdateDateColumn()
  @Exclude()
  date_update: Date;

  @ManyToOne(() => ProductEntity, (product) => product.skus)
  product: ProductEntity;
}
