import {
  Column,
  CreateDateColumn,
  Entity,
  Generated,
  Index,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { ProductEntity } from './product.entity';
import { Exclude } from 'class-transformer';

@Entity()
export class CategoryEntity {
  @PrimaryGeneratedColumn()
  @Exclude()
  id: number;

  @Column('varchar', { nullable: false, unique: true })
  @Index('idx_name_category')
  name: string;

  @Column('varchar', { nullable: true })
  description: string;

  @Column('uuid', { nullable: false })
  @Index('idx_guid_category')
  @Generated('uuid')
  guid: string;

  @Exclude()
  @Column('boolean', { default: false })
  visible: boolean;

  @CreateDateColumn()
  date_created: Date;
}
