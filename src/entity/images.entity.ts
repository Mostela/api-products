import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Exclude } from 'class-transformer';
import { ProductEntity } from './product.entity';

@Entity()
export class ImagesEntity {
  @PrimaryGeneratedColumn()
  @Exclude()
  id: number;

  @ManyToOne(() => ProductEntity, (product) => product.images)
  product: ProductEntity;

  @Column('text', { nullable: false })
  url: string;

  @Column('boolean', { default: true })
  @Exclude()
  visible: boolean;

  @CreateDateColumn()
  @Exclude()
  date_created: Date;
}
