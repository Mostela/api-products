import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  Index,
  OneToMany,
  UpdateDateColumn,
  Generated,
  ManyToOne,
  JoinTable,
  JoinColumn,
} from 'typeorm';
import { CategoryEntity } from './category.entity';
import { Exclude } from 'class-transformer';
import { SkuEntity } from './sku.entity';
import { ImagesEntity } from './images.entity';

@Entity()
export class ProductEntity {
  @PrimaryGeneratedColumn()
  @Exclude()
  id: number;

  @Column('uuid', { nullable: false })
  @Index('idx_guid_product')
  @Generated('uuid')
  guid: string;

  @Column('varchar', { nullable: false })
  name: string;

  @Column('varchar', { nullable: true })
  description: string;

  @Column('boolean', { default: false })
  @Exclude()
  visible: boolean;

  @Column('boolean', { default: true })
  @Exclude()
  in_stock: boolean;

  @ManyToOne(() => CategoryEntity, (category) => category.id, {
    nullable: false,
  })
  category: CategoryEntity;

  @OneToMany(() => SkuEntity, (sku) => sku.id)
  skus: SkuEntity[];

  @OneToMany(() => ImagesEntity, (image) => image.id, { eager: false })
  images: ImagesEntity[];

  @CreateDateColumn()
  @Exclude()
  date_created: Date;

  @UpdateDateColumn()
  @Exclude()
  date_update: Date;
}
