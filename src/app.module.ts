import { Module } from '@nestjs/common';
import { CategoryController } from './controller/category.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductEntity } from './entity/product.entity';
import { CategoryEntity } from './entity/category.entity';
import { SkuEntity } from './entity/sku.entity';
import { CategoryService } from './service/category.service';
import { ProductService } from './service/product.service';
import { ProductController } from './controller/product.controller';
import { ImagesEntity } from './entity/images.entity';
import { TerminusModule } from '@nestjs/terminus';
import { HealthController } from './controller/heatlh.controller';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.DB_HOST,
      port: 5432,
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_DATABASE,
      entities: [CategoryEntity, SkuEntity, ProductEntity, ImagesEntity],
      synchronize: true,
      autoLoadEntities: true,
    }),
    TypeOrmModule.forFeature([
      CategoryEntity,
      SkuEntity,
      ProductEntity,
      ImagesEntity,
    ]),
    TerminusModule,
  ],
  controllers: [CategoryController, ProductController, HealthController],
  providers: [CategoryService, ProductService],
})
export class AppModule {}
