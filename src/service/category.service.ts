import {
  ClassSerializerInterceptor,
  HttpCode,
  HttpException,
  HttpStatus,
  Injectable,
  UseInterceptors,
} from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { CategoryEntity } from '../entity/category.entity';
import { CreateCategoryDto } from '../dto/category';

@Injectable()
export class CategoryService {
  constructor(
    @InjectRepository(CategoryEntity)
    private categoryRepository: Repository<CategoryEntity>,
  ) {}

  async newCategory(
    data_category: CreateCategoryDto,
  ): Promise<CreateCategoryDto> {
    if (!(await this.exist_name(data_category.name))) {
      const category = this.categoryRepository.create(data_category);
      const data = await this.categoryRepository.save(category);
      if (!data)
        throw new HttpException(
          'Some problemns to save data',
          HttpStatus.BAD_REQUEST,
        );
      return category;
    }
    throw new HttpException(
      `Category with name ${data_category.name} exist`,
      HttpStatus.BAD_REQUEST,
    );
  }

  async change_visible(guid: string): Promise<boolean> {
    const category = await this.categoryRepository.findOne({
      where: { guid: guid },
    });
    category.visible = !category.visible;
    await this.categoryRepository.save(category);
    return category.visible;
  }

  async list_all_visible(): Promise<CreateCategoryDto[]> {
    return await this.categoryRepository.find({
      where: { visible: true },
    });
  }

  protected async exist_name(name: string): Promise<boolean> {
    const exist = await this.categoryRepository.findOne({
      where: { name },
    });

    return !!exist;
  }
}
