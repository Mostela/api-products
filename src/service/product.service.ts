import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CategoryEntity } from '../entity/category.entity';
import { Repository } from 'typeorm';
import { ProductEntity } from '../entity/product.entity';
import { SkuEntity } from '../entity/sku.entity';
import { ImageCreateDTO, ProductCreateDTO, SKUCreateDTO } from '../dto/product';
import { ImagesEntity } from '../entity/images.entity';

@Injectable()
export class ProductService {
  constructor(
    @InjectRepository(ProductEntity)
    private productEntityRepository: Repository<ProductEntity>,

    @InjectRepository(SkuEntity)
    private skuEntityRepository: Repository<SkuEntity>,

    @InjectRepository(CategoryEntity)
    private categoryEntityRepository: Repository<CategoryEntity>,

    @InjectRepository(ImagesEntity)
    private imagesEntityRepository: Repository<ImagesEntity>,
  ) {}

  async new_product(
    data_product: ProductCreateDTO,
    category_guid: string,
  ): Promise<ProductEntity> {
    const category = await this.return_category(category_guid);
    const product = this.productEntityRepository.create(data_product);
    product.category = category;
    await this.productEntityRepository.save(product);

    data_product.skus.map((value: SKUCreateDTO) => {
      const sku = this.skuEntityRepository.create(value);
      sku.product = product;
      this.skuEntityRepository.save(sku);
      return sku;
    });

    data_product.images.map((value: ImageCreateDTO) => {
      const image = this.imagesEntityRepository.create(value);
      image.product = product;
      this.imagesEntityRepository.save(image);
      return image;
    });

    return product;
  }

  async find_one_product(guid: string): Promise<ProductEntity> {
    const data = await this.productEntityRepository.findOne({
      where: {
        guid,
        visible: true,
      },
    });
    if (!data)
      throw new HttpException(
        'product with this guid not found',
        HttpStatus.BAD_REQUEST,
      );
    data.skus = await this.skuEntityRepository.find({
      where: { product: data, is_publish: true },
    });
    data.images = await this.imagesEntityRepository.find({
      where: { product: data, visible: true },
    });
    return data;
  }

  async get_by_category(guid_category: string) {
    const category = await this.categoryEntityRepository.findOne({
      where: {
        guid: guid_category,
        visible: true,
      },
    });
    if (!category)
      throw new HttpException('category not found', HttpStatus.BAD_REQUEST);

    const list_product = await this.productEntityRepository.find({
      where: { category: category, visible: true },
    });
    const response_list = [];
    for (const productEntity of list_product) {
      productEntity.skus = await this.skuEntityRepository.find({
        where: { product: productEntity, is_publish: true },
      });

      productEntity.images = await this.imagesEntityRepository.find({
        where: { product: productEntity, visible: true },
      });
      response_list.push(productEntity);
    }

    return response_list;
  }

  async return_category(guid: string) {
    const data = await this.categoryEntityRepository.findOne({
      where: { guid },
    });
    if (!data)
      throw new HttpException('category not found', HttpStatus.BAD_REQUEST);
    return data;
  }

  async enable_to_sell(guid: string) {
    const product = await this.productEntityRepository.findOne({
      where: { guid },
    });
    return product.in_stock;
  }
}
