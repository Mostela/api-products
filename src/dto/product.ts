import {
  ArrayMaxSize,
  ArrayMinSize,
  IsArray,
  IsBoolean,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  IsUrl,
  IsUUID,
  Max,
  MaxLength,
  Min,
  MinLength,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';

export class SKUCreateDTO {
  @IsString()
  @IsOptional()
  name: string;

  @IsString()
  @IsNotEmpty()
  @MaxLength(16)
  @MinLength(4)
  code: string;

  @IsNumber()
  @Min(0)
  old_price: number;

  @IsNumber()
  @Min(0)
  @IsNotEmpty()
  new_price: number;

  @IsBoolean()
  @IsOptional()
  is_publish: boolean;
}

export class ImageCreateDTO {
  @IsUrl()
  @IsNotEmpty()
  url: string;
}

export class ProductCreateDTO {
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  @IsOptional()
  description: string;

  @IsArray()
  @IsNotEmpty()
  @ValidateNested({ each: true })
  @ArrayMinSize(1)
  @Type(() => SKUCreateDTO)
  skus: Array<SKUCreateDTO>;

  @IsArray()
  @IsOptional()
  @ValidateNested({ each: true })
  @Type(() => ImageCreateDTO)
  @ArrayMinSize(1)
  @ArrayMaxSize(10)
  images: Array<ImageCreateDTO>;
}

export class ParamCreateProductDTO {
  @IsUUID()
  @IsNotEmpty()
  guid_category: string;
}

export class ParamFindProductDTO {
  @IsUUID()
  @IsNotEmpty()
  guid_product: string;
}
