import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  HttpCode,
  Param,
  Patch,
  Post,
  Query,
  UseInterceptors,
} from '@nestjs/common';
import { CategoryService } from '../service/category.service';
import { CreateCategoryDto, FindOndeCategoryDTO } from '../dto/category';

@Controller()
export class CategoryController {
  constructor(private readonly categoryService: CategoryService) {}

  @Post('category')
  @HttpCode(201)
  @UseInterceptors(ClassSerializerInterceptor)
  async new_category(@Body() payload: CreateCategoryDto) {
    return await this.categoryService.newCategory(payload);
  }

  @Patch('category/:guid')
  @HttpCode(202)
  async change_category_visible(@Param() params: FindOndeCategoryDTO) {
    return this.categoryService.change_visible(params.guid);
  }

  @Get('category')
  @UseInterceptors(ClassSerializerInterceptor)
  async list_all_categorys() {
    return this.categoryService.list_all_visible();
  }
}
