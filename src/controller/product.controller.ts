import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  HttpCode,
  Param,
  Post,
  UseInterceptors,
} from '@nestjs/common';
import { ProductService } from '../service/product.service';
import {
  ParamCreateProductDTO,
  ParamFindProductDTO,
  ProductCreateDTO,
} from '../dto/product';

@Controller()
export class ProductController {
  constructor(private readonly productService: ProductService) {}

  @Post('product/:guid_category')
  @HttpCode(201)
  @UseInterceptors(ClassSerializerInterceptor)
  async create_product(
    @Body() body: ProductCreateDTO,
    @Param() param: ParamCreateProductDTO,
  ) {
    return this.productService
      .new_product(body, param.guid_category)
      .then((value) => {
        return value;
      });
  }

  @Get('product/id/:guid_product')
  @HttpCode(200)
  @UseInterceptors(ClassSerializerInterceptor)
  async get_one_product(@Param() param: ParamFindProductDTO) {
    return this.productService.find_one_product(param.guid_product);
  }

  @Get('category/:guid_category')
  @HttpCode(200)
  @UseInterceptors(ClassSerializerInterceptor)
  async get_by_categorie(@Param() param: ParamCreateProductDTO) {
    return this.productService.get_by_category(param.guid_category);
  }

  @Post('product/sell/:guid_product')
  @UseInterceptors(ClassSerializerInterceptor)
  async get_in_stock(@Param() param: ParamFindProductDTO) {
    return this.productService.enable_to_sell(param.guid_product);
  }
}
