FROM node
COPY . .
RUN yarn && yarn build
EXPOSE 3000
ENTRYPOINT yarn start:prod